package main

import (
	"log/slog"
	"os"
)

// loggerBuilder ...
func loggerBuilder(logfmt *string, loglevel *string) *slog.Logger {
	var logger *slog.Logger
	var handler_options slog.HandlerOptions

	switch *loglevel {
        case "DEBUG": handler_options = slog.HandlerOptions{Level: slog.LevelDebug}
	case "INFO": handler_options = slog.HandlerOptions{Level: slog.LevelInfo}
	case "WARN": handler_options = slog.HandlerOptions{Level: slog.LevelWarn}
	case "ERROR": handler_options = slog.HandlerOptions{Level: slog.LevelError}
	}

	switch *logfmt {
	case "json": logger = slog.New(slog.NewJSONHandler(os.Stdout, &handler_options))
	default: logger = slog.New(slog.NewTextHandler(os.Stdout, &handler_options))
	}

	return logger
}
